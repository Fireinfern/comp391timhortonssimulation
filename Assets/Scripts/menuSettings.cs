using Simulation;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuSettings : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform dropdownMenu;
    public GameObject gameManager;
    public QueueSimulationManager stile;
    public InputField speed;
    [SerializeField] private ServiceTimeStrategy arrivalTimeA;
    [SerializeField] private ServiceTimeStrategy arrivalTimeB;
    [SerializeField] private ServiceTimeStrategy arrivalTimeC;
    [SerializeField] private ServiceTimeStrategy arrivalTimeD;
    [SerializeField] private ServiceTimeStrategy serviceTimeStrategyA;
    [SerializeField] private ServiceTimeStrategy serviceTimeStrategyB;
    [SerializeField] private ServiceTimeStrategy serviceTimeStrategyC;
    [SerializeField] private ServiceTimeStrategy serviceTimeStrategyD;
    private float fixedDeltaTime;
    float frametimechange = 10f;
    void Start()
    {
        this.fixedDeltaTime = Time.fixedDeltaTime;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        int menuIndex = dropdownMenu.GetComponent<Dropdown>().value;
        setType(menuIndex);
        if (speed.text.Length > 0)
        {
            float f = Single.Parse(speed.text);

            Time.timeScale = frametimechange;
        }
        
    }

    private void setType(int menuIndex)
    {
        switch(menuIndex)
        {         
            case 0: stile.arrivalTimeStrategy = arrivalTimeA;
                stile.serviceTimeStrategy = serviceTimeStrategyA;
                break;
            case 1:
                stile.arrivalTimeStrategy = arrivalTimeB;
                stile.serviceTimeStrategy = serviceTimeStrategyB;
                break; 
            case 2:
                stile.arrivalTimeStrategy = arrivalTimeC;
                stile.serviceTimeStrategy = serviceTimeStrategyC;
                break;
            case 3:
                stile.arrivalTimeStrategy = arrivalTimeD;
                stile.serviceTimeStrategy = serviceTimeStrategyD;
                break;

        }
       

    }

    public void ResetLevel()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
