using System.Collections;
using System.Collections.Generic;
using Simulation;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SubmitButtonHandler : MonoBehaviour
{
    public InputField AUniformX;
    public InputField AUniformY;
    public InputField AExponentialRate;
    public InputField ATriangularX;
    public InputField ATriangularY;
    public InputField ATriangularZ;

    public InputField SUniformX;
    public InputField SUniformY;
    public InputField SExponentialRate;
    public InputField STriangularX;
    public InputField STriangularY;
    public InputField STriangularZ;

    public Button submitButton; // Reference to the submit button

    [SerializeField] private UniformIntervalTimeStrategy arrivalUniformStrategy;
    [SerializeField] private TriangularDistributionStrategy arrivalTriangularStrategy;
    [SerializeField] private ExponentialIntervalTimeStrategy arrivalExponentialStrategy;
    
    [SerializeField] private UniformIntervalTimeStrategy serviceUniformStrategy;
    [SerializeField] private TriangularDistributionStrategy serviceTriangularStrategy;
    [SerializeField] private ExponentialIntervalTimeStrategy serviceExponentialStrategy;

    [SerializeField] private Canvas canvas;
    
    void Start()
    {
        // Add a listener to the button to trigger the `OnSubmit` method when clicked
        submitButton.onClick.AddListener(OnSubmit);
    }

    void OnSubmit()
    {
        // Validate and print the values from the input fields to the console
        float FAUniformX = ReturnValidValue(AUniformX);
        float FAUniformY = ReturnValidValue(AUniformY);
        float FAExponentialRate = ReturnValidValue(AExponentialRate);
        float FATriangularX = ReturnValidValue(ATriangularX);
        float FATriangularY = ReturnValidValue(ATriangularY);
        float FATriangularZ = ReturnValidValue(ATriangularZ);
        float FSUniformX = ReturnValidValue(SUniformX);
        float FSUniformY = ReturnValidValue(SUniformY);
        float FSExponentialRate = ReturnValidValue(SExponentialRate);
        float FSTriangularX = ReturnValidValue(STriangularX);
        float FSTriangularY = ReturnValidValue(STriangularY);
        float FSTriangularZ = ReturnValidValue(STriangularZ);

        if (FAUniformX > 0 && FAUniformX<FAUniformY)
        {
            arrivalUniformStrategy.SetNewInterval(FAUniformX, FAUniformY);
        }
        if (FAExponentialRate > 0)
        {
            arrivalExponentialStrategy.SetServiceRatePerHour(FAExponentialRate);
        }
        if (FATriangularX > 0 && FATriangularX<FATriangularY && FATriangularY<FATriangularZ)
        {
            arrivalTriangularStrategy.SetTriangleVariables(FATriangularX, FATriangularY, FATriangularZ);
        }

        if (FSUniformX > 0 && FSUniformX<FSUniformY)
        {
            serviceUniformStrategy.SetNewInterval(FSUniformX, FSUniformY);
        }
        if (FSExponentialRate > 0)
        {
            serviceExponentialStrategy.SetServiceRatePerHour(FSExponentialRate);
        }
        if (FSTriangularX > 0 && FSTriangularX<FSTriangularY && FSTriangularY<FSTriangularZ)
        {
            serviceTriangularStrategy.SetTriangleVariables(FSTriangularX, FSTriangularY, FSTriangularZ);
        }

        // Start the game with the above set paraeters for Arrival and Service
        SceneManager.LoadScene("SampleScene");
    }

    // Method to validate the input field's text and print
    private float ReturnValidValue(InputField field)
    {
        if (
            field.text.Trim().Length == 0
            || !float.TryParse(field.text, out float value)
            || value < 0
        )
        {
            field.text = ""; // Clear the field if not valid inout
            return -1f;
        }
        else
        {
            return float.Parse(field.text);
        }
    }
}
