﻿using UnityEngine;

namespace Simulation
{
    [CreateAssetMenu(fileName = "UniformIntervalTimeStrategy", menuName = "ArrivalTimeStrategy/UniformIntervalTimeStrategy", order=2)]
    public class UniformIntervalTimeStrategy : ServiceTimeStrategy
    {
        [SerializeField] 
        private Vector2 interArrivalTimeInSecondsInterval = new (0.0f, 1.0f);

        public void SetNewInterval(float a, float b)
        {
            interArrivalTimeInSecondsInterval = new Vector2(a, b);
        }
        
        public override float GetInterArrivalTime()
        {
            return Random.Range(interArrivalTimeInSecondsInterval.x, interArrivalTimeInSecondsInterval.y);
        }
    }
}