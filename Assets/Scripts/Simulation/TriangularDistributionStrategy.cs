﻿using UnityEngine;

namespace Simulation
{
    [CreateAssetMenu(fileName = "TriangularDistributionStrategy", menuName = "ArrivalTimeStrategy/TriangularDistributionStrategy", order=4)]
    public class TriangularDistributionStrategy : ServiceTimeStrategy
    {

        [SerializeField] private Vector3 triangleVariables = new Vector3(3.0f, 7.0f,5.0f);

        public void SetTriangleVariables(float a, float b, float c)
        {
            triangleVariables = new Vector3(a, b, c);
        }
        
        public override float GetInterArrivalTime()
        {
            return Utilities.GetTriangularDistribution(Random.value, triangleVariables);
        }
    }
}