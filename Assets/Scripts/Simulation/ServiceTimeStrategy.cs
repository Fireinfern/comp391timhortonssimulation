﻿using UnityEngine;

namespace Simulation
{
    public class ServiceTimeStrategy : ScriptableObject
    {
        public virtual float GetInterArrivalTime()
        {
            return 0.0f;
        }
    }

    
}