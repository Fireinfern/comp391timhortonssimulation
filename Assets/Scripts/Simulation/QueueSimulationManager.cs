﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Simulation
{
    public class QueueSimulationManager : MonoBehaviour
    {
        [SerializeField]
        public ServiceTimeStrategy serviceTimeStrategy;

        [SerializeField] public ServiceTimeStrategy arrivalTimeStrategy;

        [SerializeField] private Transform spawnTransform;

        [SerializeField] private Transform counterTransform;

        [SerializeField] private Transform exitTransform;

        [SerializeField] private GameObject customerObject;

        [SerializeField] private int maxAmountOfCustomersInQueue = 6;

        private Queue<CustomerController> _customersQueue = new Queue<CustomerController>();

        public static QueueSimulationManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            customerObject.GetComponent<CustomerController>()?.SetTransforms(counterTransform, exitTransform);
            StartCoroutine(SpawnLoop());
        }

        public void StartGame()
        {
            customerObject.GetComponent<CustomerController>()?.SetTransforms(counterTransform, exitTransform);
            StartCoroutine(SpawnLoop());
        }

        public void SetArrivalStrategy(ServiceTimeStrategy timeStrategy)
        {
            arrivalTimeStrategy = timeStrategy;
        }

        public void SetServiceStrategy(ServiceTimeStrategy timeStrategy)
        {
            serviceTimeStrategy = timeStrategy;
        }

        private bool IsQueueFull()
        {
            return _customersQueue.Count >= maxAmountOfCustomersInQueue;
        }

        public bool DoesQueueHasSpace()
        {
            return _customersQueue.Count < maxAmountOfCustomersInQueue;
        }

        private IEnumerator SpawnLoop()
        {
            while (true)
            {
                while (_customersQueue.Count < maxAmountOfCustomersInQueue)
                {
                    yield return new WaitForSeconds(arrivalTimeStrategy.GetInterArrivalTime());
                    var newCustomer = Instantiate(customerObject, spawnTransform);
                    _customersQueue.Enqueue(newCustomer.GetComponent<CustomerController>());
                }

                yield return new WaitUntil(DoesQueueHasSpace);
            }
        }

        public void StartServicingCustomer(CustomerController customerController)
        {
            if (customerController != _customersQueue.Peek())
            {
                Debug.LogError("You are trying to Start Servicing a customer that is not in front of the line");
                return;
            }

            StartCoroutine(ServicingCustomer());

        }

        private IEnumerator ServicingCustomer()
        {
            float intervalTime = serviceTimeStrategy.GetInterArrivalTime();
            yield return new WaitForSeconds(intervalTime);
            DequeueCustomer();
        }
        
        public void DequeueCustomer()
        {
            var customerController = _customersQueue.Dequeue();
            customerController.FinishServing();
        }
    }
}