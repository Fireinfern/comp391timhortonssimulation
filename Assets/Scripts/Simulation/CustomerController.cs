﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Simulation
{

    public enum CustomerState
    {
        None = 0,
        Waiting = 1,
        Serving = 2,
        Served = 4
    }
    
    public class CustomerController : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgentComponent;

        [SerializeField] private Transform counterTransform;

        [SerializeField] private Transform exitTransform;

        [SerializeField] private float raycastDistance = 1.5f;
        
        private Ray _detectionRay;

        private CustomerState _customerState = CustomerState.Waiting;
        
        public CustomerState State => _customerState;

        public void SetTransforms(Transform newCounterTransform, Transform newExitTransform)
        {
            counterTransform = newCounterTransform;
            exitTransform = newExitTransform;
        }
        
        private void Start()
        {
            _navMeshAgentComponent = GetComponent<NavMeshAgent>();
            Debug.Assert(_navMeshAgentComponent != null);
            _navMeshAgentComponent.SetDestination(counterTransform.position);
            _detectionRay = new Ray(transform.position + (Vector3.up * 0.8f), transform.forward);
        }

        private void Update()
        {
            DetectCustomerInFront();
            if (_navMeshAgentComponent.remainingDistance <= 0.01f)
            {
                if (State == CustomerState.Served)
                {
                    Destroy(gameObject);
                    return;
                }

                if (State == CustomerState.Waiting)
                {
                    _customerState = CustomerState.Serving;
                    QueueSimulationManager.Instance.StartServicingCustomer(this);
                }
                _navMeshAgentComponent.isStopped = true;
                return;
            }
        }

        private bool DetectCustomerInFront()
        {
            if (State == CustomerState.Served) return false;
            _detectionRay.origin = transform.position + (Vector3.up * 0.8f);
            _detectionRay.direction = transform.forward;
            RaycastHit[] hits =
                Physics.SphereCastAll(_detectionRay, 1.5f, raycastDistance);
            foreach (var hit in hits)
            {
                if (hit.collider.gameObject == gameObject)
                {
                    continue;
                }

                var directionOfOther = Vector3.Dot((hit.transform.position - transform.position).normalized, transform.forward);
                if (hit.collider.CompareTag("Customer") && directionOfOther > 0.7f)
                {
                    _navMeshAgentComponent.isStopped = true;
                    return true;
                }
            }

            _navMeshAgentComponent.isStopped = false;
            return false;
        }

        public void FinishServing()
        {
            _customerState = CustomerState.Served;
            _navMeshAgentComponent.SetDestination(exitTransform.position);
            _navMeshAgentComponent.isStopped = false;
        }
    }
}