﻿using UnityEngine;

namespace Simulation
{
    [CreateAssetMenu(fileName = "ExponentialIntervalTimeStrategy", menuName = "ArrivalTimeStrategy/ExponentialIntervalTimeStrategy", order=2)]
    public class ExponentialIntervalTimeStrategy : ServiceTimeStrategy
    {
        [SerializeField] private float serviceRatePerHour = 30.0f;

        public void SetServiceRatePerHour(float newServiceRate)
        {
            serviceRatePerHour = newServiceRate;
        }
        
        public override float GetInterArrivalTime()
        {
            var lambda = 1 / serviceRatePerHour;
            return Utilities.GetExp(Random.value, serviceRatePerHour) * 60.0f * 60.0f;
        }
    }
}