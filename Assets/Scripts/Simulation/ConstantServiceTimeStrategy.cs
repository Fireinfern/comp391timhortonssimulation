﻿using UnityEngine;

namespace Simulation
{
    [CreateAssetMenu(fileName = "ConstantArrivalTime", menuName = "ArrivalTimeStrategy/ConstantArrivalTimeStrategy", order=1)]
    public class ConstantServiceTimeStrategy : ServiceTimeStrategy
    {
        [SerializeField] private float constantArrivalTimeStrategy = 60.0f;
        
        
        public override float GetInterArrivalTime()
        {
            return constantArrivalTimeStrategy;
        }
    }
}