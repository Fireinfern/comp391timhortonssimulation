﻿using UnityEngine;

internal class Utilities
{
    internal static float GetExp(float u, float lambda)
    {
        return -Mathf.Log(1 - u) / lambda;
    }
    
    internal static float GetTriangularDistribution(float u, float a, float b, float c)
    {
        
        float V = (c - a) / (b - a);
        float res = 0f;
        if (u < V)
        {
            return a + Mathf.Sqrt(u * (b - a) * (c - a));
        }
        if (u>= V)
        {
            return b - Mathf.Sqrt((1-u)* (b - a) * (b- c));
        }

        return res;
    }

    internal static float GetTriangularDistribution(float u, Vector3 vertices)
    {
        return GetTriangularDistribution(u, vertices.x, vertices.y, vertices.z);
    }
}